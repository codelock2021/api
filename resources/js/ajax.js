

$(document).ready(function() {
    $('#save_form').on('click', function(e) {
        e.preventDefault();
        var fname = $("#fname").val();
        var lname = $("#lname").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var token = $('input[name="_token"').val();

        $.ajax({
            type: 'POST',
            url: "/save_register?_token="+token,
            dataType: "json",
            // cache: false,
            // contentType: false,
            // processData: false,
            data: {
                _token:token,
                email: email,
                fname: fname,
                lname: lname,
                password: password
            },
            success:function(data) {
                
                if(data.dataResult == "fail"){
                    $(".print-error-msg").css('display','none');
                    printErrorMsg(data.dataResult.error);
                    function printErrorMsg (msg) {
                        $(".print-success-msg").css("display","none");
                        $(".print-error-msg").find("ul").html('');
                        $.each( data.errors.msg, function( key, value ) {
                            $(".print-error-msg").css('display','block');
                            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
                        });
                    }
                }else{
                    printSuccessMsg(data.success.msg);
                    function printSuccessMsg (msg) {
                        $(".print-error-msg").css("display","none");
                        $(".print-success-msg").find("ul").html('');
                        $(".print-success-msg").css('display','block');
                        $(".print-success-msg").find("ul").append('<li>'+msg+'</li>');
                        $("#registerForm")[0].reset();
                         setTimeout(function(){ 
                            window.location.href = '/login';
    
                         }, 3000);
                    }
                }
    }.bind($(this))
        });
    });
    $('#login_form').on('click', function(e) {
        e.preventDefault();
        var email = $("#email").val();
        var password = $("#password").val();
        var token = $('input[name="_token"').val();
        console.log(token);
        $.ajax({
            type: 'POST',
            url: "/login?_token="+token,
            dataType: "json",
            // cache: false,
            // contentType: false,
            // processData: false,
            data: {
                _token:token,
                email: email,
                password: password
            },
            success:function(data) {
                if(data.dataResult == "fail"){
                    $(".print-error-msg").css('display','none');
                    printErrorMsg(data.msg);
                    function printErrorMsg (msg) {
                        $(".print-success-msg").css("display","none");
                        $(".print-error-msg").find("ul").html('');
                            $(".print-error-msg").css('display','block');
                            $(".print-error-msg").find("ul").append('<li>'+msg+'</li>');
                    }
                }else{
                    printSuccessMsg(data.msg);
                    function printSuccessMsg (msg) {
                        $(".print-error-msg").css("display","none");
                        $(".print-success-msg").find("ul").html('');
                        $(".print-success-msg").css('display','block');
                        $(".print-success-msg").find("ul").append('<li>'+msg+'</li>');
                        setTimeout(function(){ 
                            window.location.href = '/home';
    
                         }, 3000);
                    }
                }
    }.bind($(this))
        });
    });
});


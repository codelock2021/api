<?php ob_start();
use App\Http\Controllers\LoginController;
?>
@include('master')
  <div id="notifDiv"></div>
{{-- @section('content') --}}

    <section>
        <div class="container mt-4">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header bg-dark text-white">
                              <h4 class="text-center">Account Login</h4>
                        </div>
                        <div class="alert alert-danger print-error-msg" style="display:none">
                            <ul></ul>
                        </div>
                        <div class="alert alert-success print-success-msg" style="display:none">
                            <ul></ul>
                        </div>
                        <div class="card-body">
                            <form method="POST"> 
                                @csrf
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email">
                                </div>
                                <div class="form-group">
                                  <label for="password">Password</label>
                                  <input type="password" id="password" name="password" class="form-control" placeholder="Enter Password">
                              </div>
    
                              <button type="submit" class="btn btn-dark btn-block" id="login_form">Login</button>
                            </form>
                        </div>
                        <div class="forgot register-footer" >
                            <span>Already have an account?</span>
                            <a href="/register">Register</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
{{-- @endsection --}}
</body>
</html>


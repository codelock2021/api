<?php ob_start();
use App\Http\Controllers\RegisterController;


?>

@include('master')
    <div id="notifDiv"></div>
    {{-- @section('content') --}}

    <section>

        <div class="container mt-4">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header bg-dark text-white">
                            <h4 class="text-center">Account Register</h4>
                        </div>
                        <div class="card-body">
                            <div class="alert alert-danger print-error-msg" style="display:none">
                                <ul></ul>
                            </div>
                            <div class="alert alert-success print-success-msg" style="display:none">
                                <ul></ul>
                            </div>
                            <form name="frm" id="registerForm">
                                @csrf
                                <div class="form-group">
                                    <label for="fname">First Name</label>
                                    <input type="text" name="fname" id="fname" class="form-control"
                                        placeholder="Enter First Name">
                                </div>

                                <div class="form-group">
                                    <label for="lname">Last Name</label>
                                    <input type="text" name="lname" id="lname" class="form-control"
                                        placeholder="Enter Last Name">
                                </div>


                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email" class="form-control"
                                        placeholder="Enter Email">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" id="password" name="password" class="form-control"
                                        placeholder="Enter Password">
                                        <label>Note :- Password should be atleast 6 characters long and should contain one number,one character and one special character</label>   
                                </div>

                                <button type="submit" class="btn btn-dark btn-block" id="save_form">Register</button>
                            </form>
                            <div class="forgot register-footer" >
                                <span>Already have an account?</span>
                                <a href="/login">Login</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- @endsection --}}
</body>

</html>

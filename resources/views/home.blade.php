<?php ob_start();
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Session;
?>
@include('master')
<section>
    <div class="container mt-4">
        <div class="row col-md-12">
            <div class="row col-md-8">
                <h1>hello , Welcome <?php echo session::get('fname'); ?> </h1>
            </div>
            <div class="row col-md-4">
              <button class="btn btn-raised btn-default m-t-15 waves-effect">
                <a href="logout" class="mega-menu xs-hide"><i class="zmdi zmdi-power"></i></a>
              </button>
            </div>
        </div>
    </div>
</section>

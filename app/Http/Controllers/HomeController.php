<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home() {
        if(Session::has('email')){
            return view('home');
        }
        return view('login');
    }
}

<?php

namespace App\Http\Controllers;
use JWTAuth;
use App\User;
use Validator;
use Illuminate\Http\Request;

class UserController extends Controller
{
   protected $users;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index()
    {
        $users = User::all();
            return response()->json([
            "success" => true,
            "message" => "Users List",
            "data" => $users
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       
        $this->validate($request, [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password'=>'required',
        ]);
        // $imageName = time().'.'.$request->img->extension();
        // //dd($imageName);
        // $request->img->move(public_path('images'), $imageName);
        $imageName = time().'.'.$request->image->extension();
        //dd($imageName);
        $request->image->move(public_path('images'), $imageName);
//dd('hello');

        $users = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone'=>$request->phone,
            'gender'=>$request->gender,
            'image'=>$imageName,
        ]);
//dd($users);
        return response()->json([
            "success" => true,
            "message" => "User created successfully.",
            "data" => $users
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
         return response()->json([
            "success" => true,
            "message" => "User retrieved successfully.",
            "data" => $user
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
public function update(Request $request, $id)
{  
    //dd($request->get('name'));
                 $users = User::find($id);
                $users->name = $request->get('name');
                $users->email = $request->get('email');
                $users->phone =$request->get('phone');
                $users->gender =$request->get('gender');
                
                if($request->hasFile('image')) {
                    $image = $request->file('image');
                    $filename = $image->getClientOriginalName();
                    $image->move(public_path('images'), $filename);
                    $users->image = $request->file('image')->getClientOriginalName();
                }
                
                //$users->image =$imageName;
                $users->update();
                return response()->json([
                        "success" => true,
                        "message" => "User update successfully.",
                        "data" => $users
                    ]);  
                
    }
    


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
       
        return response()->json([
            "success" => true,
            "message" => "User deleted successfully.",
         
           ]);  
           
    }

}

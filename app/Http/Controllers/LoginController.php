<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Register;
use Illuminate\Support\Facades\Auth;



// namespace App\Http\Controllers;
// use JWTAuth;
// use Validator;
// use Illuminate\Http\Request;
// use App\Register;
// use Illuminate\Support\Facades\DB;
class LoginController extends Controller
{
    public function login() {
        if(Session::has('email')){
            return redirect('home');
        }   
        return view('login');
    }
    public function login_check(Request $request){
        $user = Register::where('email', $request->email)->first();
        if (!$user) {
            $result['dataResult'] = "fail";
                $result['msg']= 'User is not registered!';
        }
        if (empty($request->email)||empty($request->password)) {
            $result['dataResult'] = "fail";
            $result['msg']= 'Email or Password must not be empty!';
        }
        if($user){
            if (!\Hash::check($request->password, $user->password)) {
                $result['dataResult'] = "fail";
                $result['msg']= 'Ooops! credentials do not match!';
            }
        }
       
        if (\Auth::attempt($request->only('email', 'password'))) {
            session(['email' => $request->email,'fname'=>$user->fname]);
          
            $result['dataResult'] = "success";
            $result['msg']= 'Login Successful!';
        }
        
        return response()->json($result);
    }
}
?>
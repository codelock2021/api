<?php

namespace App\Http\Controllers;
use JWTAuth;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Register;
class RegisterController extends Controller
{
    public function register() {
        return view('register');
    }
    public function save_register(Request $request)
    {
        // $request->validate([
        //     'fname' => 'required',
        //     'lname' => 'required',
        //     'email' => 'required',
        //     'password' => 'required',
        // ]);
        // return  $request->input();
        // $messages = $validator->errors();

        // $this->validate($request, [
        //     'fname' => 'required',
        //     'lname' => 'required',
        //     'email' => 'required',
        //     'password' => 'required',
        // ]);
        // echo "<pre>";
        // print_r($errors );
        // die;
        // $user = Register::where('email', $request['email'])->first();
        // if($errors) {
          
        //     return response()->json(['error' => $errors]);
        // } else {
        //     $user = new Register;
        //     $user->fname = $request['fname'];
        //     $user->lname = $request['lname'];
        //     $user->email = $request['email'];
        //     $user->password =  $request['password'];
        //     $data = $user->save();
        //     return response()->json(['success' => 'User Registered Successfully']);
        // }
       
        $validator = Validator::make($request->all(), [
            'fname' => 'required|string|between:2,100',
            'lname' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'
        ]);
     
        if ($validator->passes()) {
            $user = new Register;
                $user->fname = $request['fname'];
                $user->lname = $request['lname'];
                $user->email = $request['email'];
                $user->password =  \Hash::make($request['password']);
                $data = $user->save();
                session(['email' => $request->email,'fname'=>$user->fname]);
                $result['dataResult'] = "success";
                $result['success']['msg']= 'User Registered Successfully';
        }else{
            $result['dataResult'] = "fail";
            $result['errors']['msg']= $validator->errors()->all();
            
        }
        return response()->json($result);
        
    }

}
?>
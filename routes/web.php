<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('register',"RegisterController@register");
// Route::post('register', [RegisterController::class, 'register'])->name('save_register');

Route::post('save_register',"RegisterController@save_register");

Route::get('login',"LoginController@login");
Route::post('login',"LoginController@login_check");
Route::get('home',"HomeController@home");
// Route::get('/logout',"LoginController@login");
Route::get('/logout', function () {
    session()->forget('email');
    session()->flush();
    if(Session::has('email')){
        session()->pull('email');
    }
    return redirect('login');
});
// Route::get('/login', function () {

//     if(Session::has('email')){
//         return redirect('home');
//     }
//     return view('login');
// });
// Route::get('/home', function () {

//     if(Session::has('email')){
//         return redirect('home');
//     }
//     return view('login');
// });